package reflection;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Objects;

/**
 * Created by mkory on 10/17/2015.
 */
public class ReflectionTest {

    public static void main(String[] args) throws IllegalAccessException, NoSuchFieldException {
        Class c = Person.class;
        System.out.println(Arrays.toString(c.getDeclaredFields()));


        Person person = new Person();
        Field myField = c.getDeclaredField("id");
        myField.setAccessible(true);
        Object value = myField.get(person);

        System.out.println(Arrays.toString(myField.getAnnotations()));

        System.out.println(value);


    }


}
