package dan.multiplexer;

/**
 * Created by mkory on 10/11/2015.
 */
public class InternetFilter implements ChainItem {
    ChainItem nextChainItem;

    public String multiplex(String message) {
        message = "mux(\"IP1:" + message;
        message += ("\")");
        System.out.println("otherSolution.Internet: " + message);
        if (nextChainItem != null) {
            nextChainItem.multiplex(message);
        }
        return message;
    }

    public String demultiplex(String message) {
        message = "Port2:" + message;
        System.out.println("otherSolution.Internet: " + message);
        if (nextChainItem != null) {
            nextChainItem.multiplex(message);
        }
        return message;

    }

    public void setNexFilter(ChainItem abstractTCP) {
    this.nextChainItem = abstractTCP;
    }
}
