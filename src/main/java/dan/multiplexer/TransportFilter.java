package dan.multiplexer;

/**
 * Created by mkory on 10/11/2015.
 */
public class TransportFilter implements ChainItem {
    ChainItem nextChainItem;

    public String multiplex(String message) {
        message = "mux(\"Port1:" + message;
        message += ("\")");
        System.out.println("otherSolution.Transport: " + message);
        if (nextChainItem != null) {
            nextChainItem.multiplex(message);
        }
        return message;
    }

    public String demultiplex(String message) {
        message = "From Joel>" + message;
        message += ("\")");
        System.out.println("otherSolution.Transport: " + message);
        if (nextChainItem != null) {
            nextChainItem.demultiplex(message);
        }
        return message;
    }

    public void setNexFilter(ChainItem abstractTCP) {
        this.nextChainItem = abstractTCP;
    }
}
