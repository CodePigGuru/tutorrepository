package dan.multiplexer;

/**
 * Created by mkory on 10/11/2015.
 */
public class ApplicationFilter implements ChainItem {
    ChainItem nextChainItem;

    public String multiplex(String message) {
        message = "From Sarah >" + message;
        System.out.println("otherSolution.Application: "+message);
        if(nextChainItem!=null){
            nextChainItem.multiplex(message);
        }
        return message;
    }

    public String demultiplex(String message) {

        message = "Not much" + message;
        System.out.println("otherSolution.Application:" + message);
        if(nextChainItem!=null){
            nextChainItem.demultiplex(message);
        }
        return message;

    }

    public void setNexFilter(ChainItem nextChainItem) {
    this.nextChainItem = nextChainItem;
    }

}
