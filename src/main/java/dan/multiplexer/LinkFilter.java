package dan.multiplexer;

/**
 * Created by mkory on 10/11/2015.
 */
public class LinkFilter implements ChainItem {
    ChainItem nextChainItem;

    public String multiplex(String message) {
        message = "send" + "(\"" + "MAC1:" + message;
        message += "\"";
        System.out.println("otherSolution.Link:" + message);
        if (nextChainItem != null) {
            nextChainItem.multiplex(message);
        }
        return message;
    }

    public String demultiplex(String message) {
        return null;

    }

    public void setNexFilter(ChainItem abstractTCP) {
        this.nextChainItem = abstractTCP;
    }
}
