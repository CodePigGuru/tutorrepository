package dan.multiplexer;

/**
 * Created by mkory on 10/11/2015.
 */
public interface ChainItem {

    public String multiplex(String message);

    public String demultiplex(String message);

    public void setNexFilter(ChainItem abstractTCP);

}
