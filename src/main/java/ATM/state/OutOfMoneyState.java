package ATM.state;

import ATM.ATMMachine;

/**
 * Created by mkory on 10/17/2015.
 */
public class OutOfMoneyState implements ATMState {
    ATMMachine atmMachine;

    public OutOfMoneyState(ATMMachine atmMachine) {
        this.atmMachine = atmMachine;
    }

    public void insertCard() {
        System.out.println("No money in machine");
        atmMachine.setAtmState(atmMachine.getNoCardState());
    }

    public void removeCard() {
        System.out.println("No money in machine");
        atmMachine.setAtmState(atmMachine.getNoCardState());
    }

    public void insertPin(int pin) {
        System.out.println("No money in machine");
        atmMachine.setAtmState(atmMachine.getNoCardState());
    }

    public double requestCash(double cashToWithdraw) {
        System.out.println("No money in machine");
        atmMachine.setAtmState(atmMachine.getNoCardState());
        return 0;
    }
}
