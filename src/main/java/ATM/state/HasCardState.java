package ATM.state;

import ATM.ATMMachine;

/**
 * Created by mkory on 10/17/2015.
 */
public class HasCardState implements ATMState {
    ATMMachine atmMachine;

    public HasCardState(ATMMachine atmMachine) {
        this.atmMachine = atmMachine;
    }

    public void insertCard() {
        System.out.println("Card is already inserted");
    }

    public void removeCard() {
        System.out.println("Thank you for using us");
        atmMachine.setAtmState(atmMachine.getNoCardState());
    }

    public void insertPin(int pin) {
        if (pin == 123){
            System.out.println("Correct pin");
            atmMachine.setAtmState(atmMachine.getAuthorizedState());
        }
        else {
            System.out.println("Wrong pin");
            atmMachine.setAtmState(atmMachine.getNoCardState());
        }

    }

    public double requestCash(double cashToWithdraw) {
        System.out.println("Enter pin first");
        return -1;
    }
}
