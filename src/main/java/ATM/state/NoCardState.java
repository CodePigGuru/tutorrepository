package ATM.state;

import ATM.ATMMachine;

/**
 * Created by mkory on 10/17/2015.
 */
public class NoCardState implements ATMState {
    ATMMachine atmMachine;

    public NoCardState(ATMMachine atmMachine) {
        this.atmMachine = atmMachine;
    }

    public void insertCard() {
        System.out.println("Card inserted");
        System.out.println("Please Enter PIN");
        atmMachine.setAtmState(atmMachine.getHasCardState());
    }

    public void removeCard() {
        System.out.println("Insert card first");
    }

    public void insertPin(int pin) {
        System.out.println("Insert card firsht");
    }

    public double requestCash(double cashToWithdraw) {
        return -1;
    }
}
