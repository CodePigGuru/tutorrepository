package ATM.state;

/**
 * Created by mkory on 10/17/2015.
 */
public interface ATMState {

    public void insertCard();

    public void removeCard();

    public void insertPin(int pin);

    public double requestCash(double cashToWithdraw);

}
