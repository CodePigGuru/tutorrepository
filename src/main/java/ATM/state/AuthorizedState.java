package ATM.state;

import ATM.ATMMachine;

/**
 * Created by mkory on 10/17/2015.
 */
public class AuthorizedState implements ATMState {
    ATMMachine atmMachine;

    public AuthorizedState(ATMMachine atmMachine) {
        this.atmMachine = atmMachine;
    }

    public void insertCard() {
        System.out.println("Card is already inserted");
    }

    public void removeCard() {
        System.out.println("Thanks for using us");
        atmMachine.setAtmState(atmMachine.getNoCardState());
    }

    public void insertPin(int pin) {
        System.out.println("Pin is already entered");

    }

    public double requestCash(double cashToWithdraw) {
        System.out.println("You are about to withdraw");

        if (atmMachine.getCashInMachine() > cashToWithdraw) {
            System.out.println("You withdraw " + cashToWithdraw);
            atmMachine.setCashInMachine(atmMachine.getCashInMachine() - cashToWithdraw);
            return cashToWithdraw;
        }
        else {
            System.out.println("Sorry we have no money, use other machine");
            atmMachine.setAtmState(atmMachine.getNoCardState());
            atmMachine.getOutOfMoneyState();
            return 0;
        }
    }
}
