package ATM;

import ATM.state.*;

/**
 * Created by mkory on 10/17/2015.
 */
public class ATMMachine {
    ATMState atmState;

    ATMState noCardState;
    ATMState hasCardState;
    ATMState authorizedState;
    ATMState outOfMoneyState;

    double cashInMachine = 20000;

    boolean correctPinEntered = false;

    public ATMMachine() {
        noCardState = new NoCardState(this);
        hasCardState = new HasCardState(this);
        authorizedState = new AuthorizedState(this);
        outOfMoneyState = new OutOfMoneyState(this);

        atmState = noCardState;

    }

    public void insertCard() {
        atmState.insertCard();
        setAtmState(hasCardState);
    }

    public void removeCard() {
        atmState.removeCard();
        setAtmState(noCardState);

    }


    public void insetPin(int pin) {
        atmState.insertPin(pin);
    }

    public void requestCash(double cash) {
        atmState.requestCash(cash);
    }

    public void setAtmState(ATMState atmState) {
        this.atmState = atmState;
    }

    public ATMState getAtmState() {
        return atmState;
    }

    public ATMState getNoCardState() {
        return noCardState;
    }

    public void setNoCardState(ATMState noCardState) {
        this.noCardState = noCardState;
    }

    public ATMState getHasCardState() {
        return hasCardState;
    }

    public void setHasCardState(ATMState hasCardState) {
        this.hasCardState = hasCardState;
    }

    public ATMState getAuthorizedState() {
        return authorizedState;
    }

    public void setAuthorizedState(ATMState authorizedState) {
        this.authorizedState = authorizedState;
    }

    public ATMState getOutOfMoneyState() {
        return outOfMoneyState;
    }

    public void setOutOfMoneyState(ATMState outOfMoneyState) {
        this.outOfMoneyState = outOfMoneyState;
    }

    public double getCashInMachine() {
        return cashInMachine;
    }

    public void setCashInMachine(double cashInMachine) {
        this.cashInMachine = cashInMachine;
    }

    public boolean isCorrectPinEntered() {
        return correctPinEntered;
    }

    public void setCorrectPinEntered(boolean correctPinEntered) {
        this.correctPinEntered = correctPinEntered;
    }
}
