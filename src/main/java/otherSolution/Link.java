package otherSolution;

import dan.multiplexer.ChainItem;

/**
 * Created by mkory on 10/11/2015.
 */
public class Link implements ChainItem {

    Internet internet = new Internet();

    String link = "MAC1:" + internet.multiplex();

    public String multiplex() {
        System.out.println(link);
        return link;
    }

    public String demultiplex(Message message) {
        return null;
    }

    public String multiplex(String message) {
        return null;
    }

    public String demultiplex(String message) {
        return null;
    }

    public void setNexFilter(ChainItem abstractTCP) {

    }
}
