package otherSolution;

import dan.multiplexer.ChainItem;

/**
 * Created by mkory on 10/11/2015.
 */
public class Transport implements ChainItem {

    Application application = new Application();
    String transport = "Port1:" + application.multiplex();


    public String multiplex() {
        System.out.println(transport);
        return transport;
    }

    public String demultiplex(Message message) {
        return null;
    }

    public String multiplex(String message) {
        return null;
    }

    public String demultiplex(String message) {
        return null;
    }

    public void setNexFilter(ChainItem abstractTCP) {

    }
}
