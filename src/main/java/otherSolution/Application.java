package otherSolution;

import dan.multiplexer.ChainItem;
import otherSolution.Message;

/**
 * Created by mkory on 10/11/2015.
 */
public class Application implements ChainItem {
    Message message = new Message();

    String application = "From Sarah>" + message.getName();

    public String multiplex() {
        System.out.println(application);
        return application;
    }

    public String demultiplex(Message message) {
        return null;
    }

    public String multiplex(String message) {
        return null;
    }

    public String demultiplex(String message) {
    return null;
    }

    public void setNexFilter(ChainItem abstractTCP) {

    }

}
